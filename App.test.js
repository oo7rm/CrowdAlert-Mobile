import './mocks/jest.mock.rn-firebase';
import './mocks/jest.mock.rn-google-button';
import './mocks/jest.mock.fetch-blob';
import thunk from 'redux-thunk';
import React from 'react';
import App from './App';

import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const rendered = renderer.create(<App />, {
    createNodeMock: (element) => {
      if (element.type === PersistGate) { return element.props.children }
      return null;
    }
  }).toJSON();
  expect(rendered).toBeTruthy();
});
