jest.mock('react-native-fetch-blob', () => {
    return {
        DocumentDir: () => {},
        fetch: () => {},
        base64: () => {},
        android: () => {},
        ios: () => {},
        config: () => {},
        session: () => {},
        fs: {
          dirs: {
            MainBundleDir: () => {},
            CacheDir: () => {},
            DocumentDir: () => {},
          },
          readFile: () => {return new Promise( (resolve, reject) => {})},
          writeFile: () => {return new Promise( (resolve, reject) => {})}
        },
        wrap: () => {},
        polyfill: () => {},
        JSONStream: () => {}
    }
});